// chrome://flags/#autoplay-policy

var stringeeClient, token, call, sound;
var userId = '';
var isAllowSound = true;
// var taskerServerURL = 'http://localhost:3000';
// var backEndServerURL = 'http://localhost:4000';
var taskerServerURL = 'http://128.199.161.30';
var backEndServerURL = 'https://webly.btaskee.com';
var backEndDetailURL = 'https://cs.btaskee.com';
function playSound() {
  try {
    if (isAllowSound) {
      setTimeout(function() {
        sound.play();
      }, 500);
    }
  } catch (error) {}
}
function stopSound() {
  try {
    sound.pause();
    sound.currentTime = 0;
  } catch (error) {}
}
function setSoundStatus() {
  if (isAllowSound) {
    $("#soundStatus").text("Đang bật nè");
    $("#btnSetSound").text("Tắt");
  } else {
    $("#soundStatus").text("Tắt rồi");
    $("#btnSetSound").text("Bật lại");
  }
}

$(document).ready(function () {
  //listen close page
  $(window).on("beforeunload", function() {
    updateCSStatus('RESET_STATUS', 'OFFLINE');
  });

  //check isWebRTCSupported
  console.log('StringeeUtil.isWebRTCSupported: ' + StringeeUtil.isWebRTCSupported());
  var url = new URL(decodeURI(window.location.href));
  var username = url.searchParams.get("username");
  $('#loggedUserId').text('CS: ' + username);
  token = url.searchParams.get("token");
  // Connect to Stringee
  if (token) {
    stringeeClient = new StringeeClient();
    settingClientEvents(stringeeClient);
    stringeeClient.connect(token);
  }

  sound = document.getElementById('voice-sound');
  setSoundStatus();
  $("#btnSetSound").click(function() {
    if (isAllowSound) {
      isAllowSound = false;
      stopSound();
    } else {
      isAllowSound = true;
    }
    setSoundStatus();
  });
});
function getTask (incomingcall) {
  try {
    var taskerUrl = taskerServerURL + '/api/get-task';
    var customData = JSON.parse(incomingcall.customDataFromYourServer);
    var phone = customData.fromUser.phone;
    var taskId = customData.taskId;
    if (phone && taskId) {
      $.ajax({
        type: "POST",
        url: taskerUrl,
        data: {taskId: taskId, phone: phone},
        success: function(res) {
          if (res.success) {
            var dataResponse = res.data;
            // set data into UI
            $('#serviceName').text(dataResponse.serviceText.vi);
            var dateTime = new Date(Date.parse(new Date(dataResponse.date)));
            var minute = dateTime.getMinutes();
            if (minute === 0) {
              minute = '00';
            }
            var time = dateTime.getHours() + ':' + minute + ' ngày ' + dateTime.getDate() + '-' + (dateTime.getMonth() + 1) + '-' + dateTime.getFullYear();
            $('#status-call').text('Ringing...');
            $('#status-call').css('display', 'block');
            $('#dateTime').text(time);
            $('#duration').text(dataResponse.duration + 'h');
            $('#address').text(dataResponse.address);
            $('#cost').text(dataResponse.cost.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') + ' VNĐ');
            $('#paymentMethod').text(dataResponse.payment.method);
            $('#taskStatus').text(dataResponse.status);
            $('#taskDetail').attr('href', backEndDetailURL + '/task-detail-by-id/' + taskId);
          } else {
            console.log('CAN NOT GET TASK: ', res);
          }
        },
        dataType: 'json'
      });
    }
  } catch (error) {
    console.log('getTask func: ', error.toString());
  }
}
function resetStatus () {
  $('#taskerName, #phoneNumber, #serviceName, #dateTime, #duration, #address, #cost, #paymentMethod, #taskStatus').text('');
  $('#status-call, .call-and-reject, .end-call, #phonering-alo-phoneIcon').hide();
  $('#profile-avatar').attr('src', 'public/images/profile.png');
  $('#taskDetail').attr('href', '#');
}
function setStatusBUSY () {
  $('#phonering-alo-phoneIcon').hide();
  $('#cs-status').css('background-color', 'red');
}
function setStatusAVAILABLE () {
  $('#cs-status').text('AVAILABLE');
  $('#cs-status').css('background-color', 'green');
}
function updateCSStatus (action, status) {
  console.log('ACTION: ', action, ' => ', status);
  try { 
    var url = backEndServerURL + '/api/support/updateCSVoiceCallStatus';
    if (userId && status) {
        $.ajax({
        type: "POST",
        url: url,
        data: {csId: userId, status: status},
        success: function(res) {
          console.log(res);
          if (Number(res) === 1) { //update successfully
            switch (action) {
              case 'ACCEPT':
                answerCall();
                $('#status-call').text('Connected!');
                $('#status-call').css('display', 'block');
                $('.call-and-reject').hide();
                $('.end-call').show();
                $('#cs-status').text('BUSY');
                setStatusBUSY();
                break;
              case 'ENDCALL':
                hangupCall();
                $('#status-call').text('Completed!');
                $('#status-call').css('display', 'block');
                $('.call-and-reject').hide();
                $('.end-call').hide();
                $('#phonering-alo-phoneIcon').hide();
                setStatusAVAILABLE();
                break;
              case 'REJECT':
                rejectCall();
                $('#status-call').text('Rejected!');
                $('#status-call').css('display', 'block');
                $('.call-and-reject').hide();
                $('.end-call').hide();
                setStatusAVAILABLE();
                $('#phonering-alo-phoneIcon').hide();
                break;
              case 'RESET_STATUS':
                setStatusAVAILABLE();
                rejectCall();
                resetStatus();
                break;
              default:
                break;
            };
          } else {
            console.log('CAN NOT UPDATE USER STATUS');
          }
        },
        dataType: 'json'
      });
    }
  } catch (error) {
    console.log('ERROR UPDATE CS STATUS: ', error.toString());
  }
}
function settingClientEvents(client) {
  client.on('connect', function () {
    console.log('connected to StringeeServer');
  });
  client.on('authen', function (res) {
    console.log('Authen');
    userId = res.userId;
    $('#profile-avatar').attr('src', './public/images/profile.png');
    setStatusAVAILABLE();
    updateCSStatus('RESET_STATUS', 'AVAILABLE');
  });
  client.on('disconnect', function () {
    console.log('disconnected');
  });
  client.on('incomingcall', function (incomingcall) {
    console.log('Incoming Call');
    call = incomingcall;
    settingCallEvents(incomingcall);
    //ring the bell
    playSound();

    var customData = JSON.parse(incomingcall.customDataFromYourServer);
    var phone = customData && customData.fromUser && customData.fromUser.phone ? customData.fromUser.phone : '';
    var avatar = customData && customData.fromUser && customData.fromUser.avatar ? customData.fromUser.avatar : '';
    if (!avatar || avatar === '/avatars/avatarDefault.png') {
      avatar = './public/images/profile.png';
    }
    $('#taskerName').text(incomingcall.fromAlias);
    $('#phoneNumber').text(phone);
    $('#profile-avatar').attr('src', avatar);
    $('.call-and-reject').css('display', 'flex');
    $('.call-and-reject').show();
    $('.end-call').hide();
    $('#cs-status').text('BUSY');
    $('#cs-status').css('background-color', 'red');
    getTask(incomingcall);
  });
  client.on('requestnewtoken', function () {
    console.log('request new token; please get new access_token from YourServer and call client.connect(new_access_token)');
    //please get new access_token from YourServer and call: 
    //client.connect(new_access_token);
  });
  client.on('otherdeviceauthen', function (data) {
    console.log('otherdeviceauthen: ', data);
  });
}
function settingCallEvents(call1) {
  call1.on('addlocalstream', function (stream) {
  });
  call1.on('addremotestream', function (stream) {
    // reset srcObject to work around minor bugs in Chrome and Edge.

    remoteVideo.srcObject = null;
    remoteVideo.srcObject = stream;
  });
  call1.on('signalingstate', function (state) {
    console.log('signalingstate ', state);
    if (state.code == 6) {
      updateCSStatus('ENDCALL', 'AVAILABLE');
      stopSound();
    }
    var reason = state.reason;
    $('#callStatus').text(reason);
  });
  call1.on('mediastate', function (state) {
    console.log('mediastate ', state);
  });
  call1.on('info', function (info) {
    console.log('on info', info);
  });
  call1.on('otherdevice', function (data) {
    console.log('on otherdevice:' + JSON.stringify(data));
  });
}
function answerCall() {
  call && call.answer(function (res) {
    console.log('answer res', res);
  });
  stopSound();
}
function rejectCall() {
  call && call.reject(function (res) {
    console.log('reject res', res);
  });
  stopSound();
}
function hangupCall() {
  remoteVideo.srcObject = null;
  call && call.hangup(function (res) {
    // console.log('hangup res', res);
  });
  stopSound();
}